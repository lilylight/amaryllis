/*
 * $Id: LogD.java,v 1.1 2012/04/21 00:09:00 rug Exp $
 * Copyright (C) 2012
 * Takuya Naraoka (lilylight.android@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either versions 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAKASI, see the file COPYING.  If not, write to the Free
 * Software Foundation Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

package jp.lilylight.amaryllis;

import android.util.Log;

public class LogD {

	public static String TAG = "Lilylight";

	public static void e(String msg) {
		if (BuildConfig.DEBUG)
			Log.e(TAG, msg);
	}
	
	public static void e(String msg, Throwable tr) {
		if (BuildConfig.DEBUG)
			Log.e(TAG, msg, tr);
	}

	public static void w(String msg) {
		if (BuildConfig.DEBUG)
			Log.w(TAG, msg);
	}
	
	public static void w(Throwable tr) {
		if (BuildConfig.DEBUG)
			Log.w(TAG, tr);
	}
	
	public static void w(String msg, Throwable tr) {
		if (BuildConfig.DEBUG)
			Log.w(TAG, msg, tr);
	}

	public static void i(String msg) {
		if (BuildConfig.DEBUG)
			Log.i(TAG, msg);
	}
	
	public static void i(String msg, Throwable tr) {
		if (BuildConfig.DEBUG)
			Log.i(TAG, msg, tr);
	}

	public static void d(String msg) {
		if (BuildConfig.DEBUG)
			Log.d(TAG, msg);
	}
	
	public static void d(String msg, Throwable tr) {
		if (BuildConfig.DEBUG)
			Log.d(TAG, msg, tr);
	}

	public static void v(String msg) {
		if (BuildConfig.DEBUG)
			Log.v(TAG, msg);
	}
	
	public static void v(String msg, Throwable tr) {
		if (BuildConfig.DEBUG)
			Log.v(TAG, msg, tr);
	}

}
